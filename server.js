var http = require('http');
var fs = require('fs');

http.createServer(function (req, res) {
    let address =  "/"+req.url.split("/")[1];
    let last = req.url.split("/")[2];
    switch(address) {
        case '/html':
            if(last===undefined){
                fs.readFile('./html.html', function(err, data) {
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.write(data);
                return res.end();
              });
            } else{
                res.writeHead(404, {'Content-Type': 'text/html'});
                res.write("error");
                return res.end();
            }
            break;
        case '/json':
            if(last==undefined){
                fs.readFile('./json.json', function(err, data) {
                    res.writeHead(200, {'Content-Type': 'application/json'});
                    res.write(data);
                    return res.end();
                  });
            }
            else{
                res.writeHead(404, {'Content-Type': 'text/html'});
                res.write("error");
                return res.end();
            }
            break;
        case '/uuid':
            if(last==undefined){
            fs.readFile('./fileUID.json', function(err, data) {
                res.writeHead(200, {'Content-Type': 'application/json'});
                res.write(data);
                return res.end();
              });
            } else{
                res.writeHead(404, {'Content-Type': 'text/html'});
                res.write("error");
                return res.end();
            }
            break;
        case '/status':
            res.writeHead(last, {'Content-Type': 'text/html'});
            res.write("response "+last);
            res.end();
            break;
        case '/delay':
            setTimeout(()=>{
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.write("delay "+last+" second");
                res.end();
            },last*1000);
            break;
        default:
            res.writeHead(404, {'Content-Type': 'text/html'});
            res.write("error");
            return res.end();
    }
}).listen(8080);